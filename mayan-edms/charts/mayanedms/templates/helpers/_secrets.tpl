{/* vim: set filetype=mustache: */}}

{{- define "mayan.secretsName" -}}
{{- printf "mayan-edms-secrets" -}}
{{- end }}

{{/*
Generate extra secrets, not managed by the _helpers
*/}}
{{- define "mayan.secrets.unmanaged" -}}
{{- range $key, $val := .Values.secrets -}}
{{ $key }}: {{ $val | quote }}
{{- "\n" -}}
{{- end -}}
{{- end -}}
